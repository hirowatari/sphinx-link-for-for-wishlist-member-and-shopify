Install sphinx (apt install python-sphinx)

Build the docs locally (in the docs directory) `make html`.

Locally the images are not copied into the build. Run `cp -r docs/source/images/* docs/build/html/_images/` to view images.

View the output in `docs/build/html`

Documents are published to [Read the Docs](http://docs.shopifywishlistmember.com/) when pushed.

