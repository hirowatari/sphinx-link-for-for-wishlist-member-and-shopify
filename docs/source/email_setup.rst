.. _email_setup:

Email Setup
===============

Default Email Settings
----------------------

By default emails will be sent from `notifications@shopifywishlistmember.com` via Amazon's Simple Email Service.  The email will look like:

   Thank you for you order for Gold Membership. To access your product
   please click the link below.

   https://example.com/123

Recipients can reply to your Shopify admin's email address.

Mailjet Settings
----------------
You can use this to send customized emails from your own domain using Mailjet.  This will allow you to customize the design and the sending address of the email.

.. warning::
  If you enable this, but do not correctly set it up, then your customers will not receive their registration emails.

.. note::
  This is an optional feature. If you do not activate it your clients will get our generic email with an authenticated link.

To setup Mailjet, perform the following.

#. Create an account with `mailjet.com <https://app.mailjet.com/signup>`_. You will be able to send up to 6000 emails/month for free.

#. Setup a transactional email template in Mailjet. There is `documentation on Mailjet <https://app.mailjet.com/docs/template_builder_transactional>`_ to guide you through this. You can use the following variable in your template:

   a. **Mandatory:** you must include a link in your email that contains ``{{var:registration_link:""}}`` so that the authenticated link is sent to the customer correctly.

    .. note::
      To create a link use: ``<a href='{{var:registration_link:""}}'>registration link</a>``
      To display the link in plain text use: ``{{Escape(var:registration_link:"")}}``

    .. warning::
      If you do not Escape the registration link some email clients will cause it be malformed.

   b. Optional: ``{{var:first_name:""}}`` can be used to include the client’s name and ``{{var:product_name:""}}`` can be used to include the Shopify product name.

#. Setup the `Mailjet Settings` in `Link for Wishlist Member and Shopify`.

   a. Fill out the `API Public Key` and the `API Private Key` in `Link for Wishlist Member and Shopify` from the credentials in the `transactional tab in Mailjet <https://app.mailjet.com/transactional>`_.

    .. figure:: images/mailjet-api-key.png
       :scale: 75 %

   b. Fill out the `From Address` in `Link for Wishlist Member and Shopify`. This must be setup as a `transactional email address in Mailjet <https://app.mailjet.com/account/sender>`_.

   c. Fill out the `Template ID` in `Link for Wishlist Member and Shopify`.  To find this, in Mailjet go to the `Transactional` tab and choose the `My Transactional Templates <https://app.mailjet.com/templates/transactional>`_  sub tab. Click on the desired template then copy the `Template ID` into your shopify app.

      .. figure:: images/mailjet-template-id.png
         :scale: 75 %


#. In `Link for Wishlist Member and Shopify` click `Save and switch to sending emails with Mailjet`.

#. Send a test email to ensure that you set everything up correctly. This is very important because if you did not set things up correctly your customers will not receive their registration emails.
