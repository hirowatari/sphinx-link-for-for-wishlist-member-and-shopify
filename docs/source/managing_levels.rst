.. _managing-levels:

Managing Levels
===============

**Walk Through**

Let's say you want customers who purchase your Shopify product `Marble Wallet` to receive a `Gold` membership in WishList Member.

#. Select `Marble Wallet` from the first drop down.
#. Select `Gold` from the second drop down.
#. Click `Add`.  You should see a confirmation message.

.. figure:: images/managing-levels-relationship-setup.png
   :scale: 75 %
   :align: right


From now on, when a customer purchases a Marble Wallet, they will receive an email with an authenticated link.  They can use this link to create a
   new Wishlist Member account or login to an existing account.


Frequently Asked Questions
--------------------------

**What happens when I click the `Remove` button beside a level?**

Future customers who purchase `Marble Wallet` will not get access to a `Gold` membership.  However, customers who already purchased will retain access.  If a customer purchased `Marble Wallet` but has not yet clicked the authenticated link, they will still be able to.

**What if I want to delete a membership level in WishList Member?**

You must first delete the relationship in Shopify. If you don't do this first, then your customers will still get an email asking them to login.  If they already have an authenticated link to that level, they will see an error message letting them know that level is gone.

**What happens if I update the name of a level in WishList Member?**

Customers will get access to that new level.

**Help! I accidentally deleted a level in WishList Member!**

If you have any customers who have purchased a level, but not redeemed it, they will see an error message informing them that level has been deleted.  For future customers, you'll need to setup the relationship again and remove the old one.

**What do I do if I find a bug?**

Email me and let me know.

**I really need a new feature**

This app is being actively maintained and I'm looking for feedback.  I'd love it if you emailed me to let me know.

**Well what's your email then?**

Kenton@shopifywishlistmember.com.

**Is this app good?**

Yes.
