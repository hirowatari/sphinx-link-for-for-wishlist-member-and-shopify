Link for Wishlist Member
====================================
Setup Guide and Documentation for the `Shopify App <https://apps.shopify.com/link-for-wishlist-member>`_.

.. toctree::
   :maxdepth: 2

   initial_setup
   managing_levels
   email_setup
   auto_add

.. note::

   If you have any question or feedback about the app, I would love to hear from you.  Email kenton@shopifywishlistmember.com.
