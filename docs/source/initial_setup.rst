Initial Setup
=============

You'll need to setup both the WordPress plugin and the Shopify app. The WordPress plugin must be installed first.

.. note::

   If you have any question about setting up the app, I would love to hear from you and be happy to help.  Email kenton@shopifywishlistmember.com.

Setup WordPress Plugin
----------------------

#. Install the `Link for Shopify and Wishlist Member <https://wordpress.org/plugins/link-for-shopify-and-wishlist-member/>`_ WordPress plugin. This should be done in the same way as you installed WishList Member. `Here <https://www.youtube.com/watch?v=AXM1QgMODW0>`_ is a simple video tutorial. However, there are a few different ways to install a plugin, see WordPress' `Managing Plugins <https://codex.wordpress.org/Managing_Plugins>`_ page.

#. If you haven't already, setup a level in WishList Member by navigating to WishList Member -> Levels
#. Navigate to WishList Member -> Integration.
#. You'll see one of two things:

   a. If you've never setup a shopping cart before, then you'll see a list of shopping carts. Choose `Generic` and click `Update Shopping Carts`.

      .. figure:: images/wordpress-setup-list-of-carts.png
         :scale: 75 %

   b. If you've setup a shopping cart before, then you'll see a cog icon.  Click the cog icon to bring up the list of carts. Choose `Generic` and click `Update Shopping Carts`.

      .. figure:: images/wordpress-setup-cog.png
         :scale: 75 %

#. After either 4a. or 4b. choose `Generic` from the dropdown and click `Set Shopping Cart`.

.. image:: images/wordpress-setup-set-cart.png

.. admonition:: Congratulations

   You've now completed the setup process for the WordPress Plugin. The next part will be easier.

Setup Shopify App
-----------------

1. Install the `Shopify App <https://apps.shopify.com/link-for-wishlist-member>`_. Once installed, you should be on the app home page. If not, navigate to Apps -> Link for Wishlist Member.  You should now see:

.. figure:: images/shopify-setup-step-1.png

2. Put in the URL of WordPress site eg ``http://www.yoursite.com``.  If you're unsure of the exact url, login to your WordPress site.  You should be at a URL that ends in ``wp-admin``.  Put in the URL that you are on omitting the ``wp-admin``.  For example, if you are one ``http://example.com/members/wp-admin`` use the URL ``http://exampe.com/members/``.  You should now see a confirmation message.

.. figure:: images/shopify-setup-step-2.png

3. You can now assign WishList Member levels to Shopify products using the drop down options.  A customer who purchase the related Shopify Product will receive an email with an authenticated link that allows them to create a WishList Member account.

Assigning WishList Member levels to Shopify products is mostly straightforward, but for more detailed information see :ref:`managing-levels`.
