.. _auto_add:

Auto Add
===============

Auto add is off by default.  You can enable it in WordPress within the plugin.

If it is enabled, users with emails in Shopfiy that exactly match the email in WordPress will

#. Automatically be logged in
#. Have their new level added to their account